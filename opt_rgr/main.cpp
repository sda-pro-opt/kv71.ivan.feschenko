#include "Lexer.h"
#include "InformationTables.h"
#include "Parser.h"
#include "Translator.h"
#include <iostream>
#include <cstdlib>

int main()
{
	Translator translator;
	translator.translation();
	return 0;
}