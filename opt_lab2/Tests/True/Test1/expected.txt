 1   1  301   PROGRAM
 1   9  401        PR
 1  11   59         ;
 2   1  304       VAR
 2   5  402         A
 2   6   58         :
 2   7  306   INTEGER
 2  14   59         ;
 2  16  403         B
 2  17   58         :
 2  18  305     FLOAT
 2  23   59         ;
 3   1  302     BEGIN
 4   1  307        IF
 4   4  403         B
 4   6   61         =
 4   8  402         A
 4  10  309      THEN
 5   1  308     ENDIF
 5   6   59         ;
 6   1  303       END
 6   4   46         .

Identifiers table
-------------------------------------
|              PR |             401 |
|               A |             402 |
|               B |             403 |
-------------------------------------

Constants table
-------------------------------------
-------------------------------------

1-<signal-program>
3---<program>
5-----PROGRAM
5-----<procedure-identifier>
7-------<identifier>
9---------PR
5-----;
5-----<block>
7-------<variable-declarations>
9---------VAR
9---------<declarations-list>
11-----------<declaration>
13-------------<variable-identifier>
15---------------<identifier>
17-----------------A
13-------------:
13-------------<attribute>
15---------------INTEGER
13-------------;
11-----------<declarations-list>
13-------------<declaration>
15---------------<variable-identifier>
17-----------------<identifier>
19-------------------B
15---------------:
15---------------<attribute>
17-----------------FLOAT
15---------------;
13-------------<declarations-list>
15---------------<empty>
7-------BEGIN
7-------<statement-list>
9---------<statement>
11-----------<condition-statement>
13-------------<incomplete-condition-statement>
15---------------IF
15---------------<conditional-expression>
17-----------------<expression>
19-------------------<variable-identifier>
21---------------------<identifier>
23-----------------------B
17-----------------=
17-----------------<expression>
19-------------------<variable-identifier>
21---------------------<identifier>
23-----------------------A
15---------------THEN
15---------------<statement-list>
17-----------------<empty>
13-------------<alternative-part>
15---------------<empty>
11-----------ENDIF
11-----------;
9---------<statement-list>
11-----------<empty>
7-------END
5-----.
DATA SEGMENT
	A DW ?
	B DW ?
DATA ENDS
CODE SEGMENT
	ASSUME CS:CODE, DS:DATA
PR:
	MOV AX, B
	MOV BX, A
	CMP AX, BX
	JNE ?L1
	NOP
?L1:    NOP
	NOP
	NOP
MOV AH, 4Ch
INT 21h
CODE ENDS
END PR
